/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_debug.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/03 19:04:08 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 16:15:13 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_struct(t_stuff *data)
{
	time_t  time;

	if (!data)
	{
		ft_printf("NULL\n");
		return ;
	}
	ft_printf("name = %s\n", data->name);
	ft_printf("path = %s\n", data->path);
	ft_printf("full_path = %s\n", data->full_path);
	ft_printf("number of links = %d\n", data->links);
	ft_printf("user = %s\n", data->user);
	ft_printf("group = %s\n", data->group);
	ft_printf("size in bytes = %d\n", data->size);
	ft_printf("number of blocs = %d\n", data->n_blocks);
	time = (time_t)data->time;
	ft_printf("time = %s\n", ctime(&time));
}

void	ft_print_params(char **argv, int argc)
{
	int	i;

	i = 1;
	if (argc > 0)
	{
		while (i < argc)
			ft_putendl(argv[i++]);
	}
}

void                    flags_reader(int flags)
{
	int    n;
	int    i;
	char **strings = (char *[]){"listdot", "longform", "recursive", "reversesort", "timesort", "accesstime", "listdir", "nosort", "acl", "extatt", "grouponly", "nogroup", "flags", "humanval", "inode", "kblocks", "nonprint", "creattime", "numericonly", "octal", "octal_esc", "sectime", "singlecol", "size", "slash", "sortacross", "statustime", "stream", "sizesort", "type", "colors", "notabs"};

	n = ft_power(2, 31);
	i = 0;
	while (n > 0)
	{
		if (flags >= n)
		{
			ft_printf("%s%s : 1\n%s", P_BLUE, strings[31 -i], NONE);
			flags = flags -n;
		}
		n = n / 2;
		i++;
	}
}

void		ops_reader(t_operands *ops)
{
	t_operands	*tmp;

	tmp = ops;
	if (!ops)
		ft_printf("ops is null\n");
	else
	{
		while (tmp)
		{
			if (tmp->skip == 2)
				ft_printf("(d)");
			else if (tmp->skip == 1)
				ft_printf("(f)");
			else
				ft_printf("(e)");
			ft_printf(" %s ", tmp->op_name);
			if (tmp->skip == 0)
				ft_printf("(skip)");
			ft_printf("\n");
			tmp = tmp->next;
		}
	}
}

void		pad_reader(t_padding *p_node)
{
	if (!p_node)
		return ;
	ft_printf(SUN);
	ft_printf("maxl_inode = %d\n", p_node->maxl_inode);
	ft_printf("maxl_n_blocks = %d\n", p_node->maxl_n_blocks);
	ft_printf("maxl_name = %d\n", p_node->maxl_name);
	ft_printf("maxl_links = %d\n", p_node->maxl_links);
	ft_printf("maxl_user = %d\n", p_node->maxl_user);
	ft_printf("maxl_group = %d\n", p_node->maxl_group);
	ft_printf("maxl_size = %d\n", p_node->maxl_size);
	ft_printf(NONE);
}

void		padding_test(void)
{
	int inode;

	inode = 6542;
	ft_printf("|%*d|\n", 3, inode);
	ft_printf("|%-6d|\n", inode);
}

void	color_test(void)
{
	ft_printf("\033[1m\033[36mdirectory\033[39;49m\033[0m");
	ft_printf("\033[30m\033[43mexecutable dir (user)\033[39;49m\033[0m");
	ft_printf("\033[35msymlink\033[39;49m\033[0m");
	ft_printf("\033[31mexecutable file\033[39;49m\033[0m");
	ft_printf("\033[34m\033[43mCHR\033[39;49m\033\[0m");
	ft_printf("\033[34m\033[46mBLK\033[39;49m\033[0m");
	ft_printf("\033[32mSOCKET\033[39;49m\033[0m");
	ft_printf("\033[33mFIFO\033[39;49m\033[0m");
	ft_printf("\n");
	ft_printf(C_NONE);
	ft_printf(C_DIR"directory"C_NONE);
	ft_printf(C_EXEC_DIR"exec dir"C_NONE);
	ft_printf(C_LNK"symlink"C_NONE);
	ft_printf(C_EXEC_FILE"exec file"C_NONE);
	ft_printf(C_CHR"chr"C_NONE);
	ft_printf(C_BLK"blk"C_NONE);
	ft_printf(C_SOCK"socket"C_NONE);
	ft_printf(C_FIFO"fifo"C_NONE);
	ft_printf("\n");
}

void	pr_whole_list_names(t_stuff *head, int flags)
{
	t_stuff	*tmp;

	tmp = head;
	while (tmp)
	{
		ls_non_printable(tmp->name, flags);
		ft_printf("\n");
		tmp = tmp->next;
	}
}
