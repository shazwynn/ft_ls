/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_structs.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/17 16:30:58 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 19:36:43 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_STRUCTS_H
# define FT_LS_STRUCTS_H

typedef struct	s_stuff
{
	char			*name;
	char			*path;
	char			*full_path;
	dev_t			devid;
	ino_t			inode;
	int				n_blocks;
	int				ret;
	mode_t			mode;
	int				is_symlink;
	char			*linking_to;
	int				links;
	char			*user;
	int				user_free;
	char			*group;
	int				group_free;
	int				size;
	time_t			time;
	long			ntime;
	struct s_stuff	*next;
	struct s_stuff	*side_next;
}				t_stuff;

typedef struct	s_operands
{
	char				*op_name;
	int					skip;
	DIR					*curr_dir;
	struct s_operands	*next;
}				t_operands;

typedef struct	s_padding
{
	int	maxl_inode;
	int	maxl_n_blocks;
	int	maxl_name;
	int	maxl_links;
	int	maxl_user;
	int	maxl_group;
	int	maxl_size;
}				t_padding;

#endif
