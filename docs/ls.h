#define NO_PRINT	1

extern long blocksize;		/* block size units */

extern int f_accesstime;	/* use time of last access */
extern int f_flags;		/* show flags associated with a file */
extern int f_humanval;		/* show human-readable file sizes */
#ifndef __APPLE__
extern int f_lomac;		/* show LOMAC attributes */
#endif /* __APPLE__ */
extern int f_inode;		/* print inode */
extern int f_longform;		/* long listing format */
extern int f_octal;		/* print unprintables in octal */
extern int f_octal_escape;	/* like f_octal but use C escapes if possible */
extern int f_nonprint;		/* show unprintables as ? */
extern int f_sectime;		/* print the real time for all files */
extern int f_size;		/* list size in short listing */
extern int f_slash;		/* append a '/' if the file is a directory */
extern int f_sortacross;	/* sort across rows, not down columns */ 
extern int f_statustime;	/* use time of last mode change */
extern int f_notabs;		/* don't use tab-separated multi-col output */
extern int f_type;		/* add type character for non-regular files */
#ifdef COLORLS
extern int f_color;		/* add type in color for non-regular files */
#endif

typedef struct {
	FTSENT *list;
	u_int64_t btotal;
	int bcfile;
	int entries;
	int maxlen;
	u_int s_block;
	u_int s_flags;
	u_int s_lattr;
	u_int s_group;
	u_int s_inode;
	u_int s_nlink;
	u_int s_size;
	u_int s_user;
} DISPLAY;

typedef struct {
	char *user;
	char *group;
	char *flags;
#ifndef __APPLE__
	char *lattr;
#endif /* __APPLE__ */
	char data[1];
} NAMES;
