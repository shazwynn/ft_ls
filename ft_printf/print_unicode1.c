/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_unicode1.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 03:09:52 by algrele           #+#    #+#             */
/*   Updated: 2018/05/19 03:15:17 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <pf_mini_libft.h>
#include <ft_printf.h>
#include <printf_tests.h>

static char	*fill_octet(int tab[32], int *n_bits, char *mask, char *octet)
{
	int		i;

	!octet ? octet = pf_strdup(mask) : pf_strcpy(octet, mask);
	if (!octet)
		return (NULL);
	i = 0;
	while (octet[i] != 'x')
		i++;
	if (i != 2)
	{
		if (*n_bits <= 11)
			i = i + 5 - *n_bits % 6;
		else if (*n_bits <= 16)
			i = i + 4 - *n_bits % 6;
		else if (*n_bits <= 21)
			i = i + 3 - *n_bits % 6;
	}
	while (i < 8)
		octet[i++] = tab[--(*n_bits)] + '0';
	i = -1;
	while (++i < 8)
		if (octet[i] == 'x')
			octet[i] = '0';
	return (octet);
}

static int	ft_putwchar_n(int tab[32], int n_octet, int n_bits)
{
	char	*octet;
	int		write_me;
	int		written;

	octet = NULL;
	if (!(written = 0) && n_octet == 2)
		octet = fill_octet(tab, &n_bits, MASK2, octet);
	else if (n_octet == 3)
		octet = fill_octet(tab, &n_bits, MASK3, octet);
	else
		octet = fill_octet(tab, &n_bits, MASK4, octet);
	if (!octet)
		return (-1);
	write_me = pf_atoi_base(octet, "01");
	write(1, &write_me, 1);
	while (++written < n_octet)
	{
		octet = fill_octet(tab, &n_bits, MASKF, octet);
		if (!octet)
			return (-1);
		write_me = pf_atoi_base(octet, "01");
		write(1, &write_me, 1);
	}
	free(octet);
	return (written);
}

int			ft_putwchar(wint_t c, int max_bytes)
{
	int	tab[32];
	int	i;

	if (c <= 127)
		return (pf_putchar(c));
	if (c <= 255 && MB_CUR_MAX == 1)
		return (pf_putchar(c - 256));
	i = 0;
	while (c)
	{
		tab[i] = c & 1;
		c = c >> 1;
		i++;
	}
	if (i <= 11 && max_bytes >= 2)
		return (ft_putwchar_n(tab, 2, i));
	else if (i <= 16 && max_bytes >= 3)
		return (ft_putwchar_n(tab, 3, i));
	else if (i <= 21 && max_bytes >= 4)
		return (ft_putwchar_n(tab, 4, i));
	return (0);
}

int			ft_putwstr(wchar_t *s, int max_bytes)
{
	int count;
	int	ret;

	count = 0;
	if (max_bytes < 0)
		return (-1);
	while (s && s[0] && max_bytes)
	{
		if (s[0] >= 256 && MB_CUR_MAX == 1)
			return (-1);
		ret = ft_putwchar(s[0], max_bytes);
		count = count + ret;
		max_bytes = max_bytes - ret;
		s++;
	}
	return (count);
}
