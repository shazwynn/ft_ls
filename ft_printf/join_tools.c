/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   join_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/27 22:18:23 by algrele           #+#    #+#             */
/*   Updated: 2018/05/18 16:18:58 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <pf_mini_libft.h>

/*
** appends the addme character once at the start of string src
** returns a new allocated joined string, and frees old src.
*/

char	*ft_join_head(char *prefix, char *src)
{
	char	*join;

	if (!src)
		return (NULL);
	join = pf_strnew(pf_strlen(prefix) + pf_strlen(src));
	if (!join)
		return (NULL);
	pf_strcpy(join, prefix);
	pf_strcpy(join + pf_strlen(prefix), src);
	pf_strdel(&src);
	return (join);
}

/*
** appends the addme character n times at the start of string src
** returns a new allocated joined string, and frees old src.
*/

char	*ft_join_n_head(char prefix, char *src, int n)
{
	char	*join;

	if (!src || n < 1)
		return (NULL);
	join = pf_strnew(n + pf_strlen(src));
	if (!join)
		return (NULL);
	pf_memset(join, prefix, n);
	pf_strcpy(join + n, src);
	pf_strdel(&src);
	return (join);
}

/*
** appends the addme character n times at the end of string src
** returns a new allocated joined string, and frees old src.
*/

char	*ft_join_n_tail(char suffix, char *src, int n)
{
	char	*join;

	if (!src || n < 1)
		return (NULL);
	join = pf_strnew(n + pf_strlen(src));
	if (!join)
		return (NULL);
	pf_strcpy(join, src);
	pf_memset(join + pf_strlen(src), suffix, n);
	pf_strdel(&src);
	return (join);
}

/*
** inserts the addme character n times in string src just after index start.
** returns a new allocated joined string, and frees old src.
*/

char	*ft_join_n_at(char addme, char *src, int start, int n)
{
	char	*join;
	int		len;
	int		total;

	if (!src || n < 1)
		return (NULL);
	len = pf_strlen(src);
	if (start > len)
		return (NULL);
	total = n + len;
	join = pf_strnew(total);
	if (!join)
		return (NULL);
	pf_strncpy(join, src, start);
	pf_memset(join + start, addme, n);
	pf_strncpy(join + start + n, src + start, len - start);
	pf_strdel(&src);
	return (join);
}
