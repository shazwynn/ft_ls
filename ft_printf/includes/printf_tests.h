/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_tests.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/28 13:22:49 by algrele           #+#    #+#             */
/*   Updated: 2018/05/19 02:51:16 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRINTF_TESTS_H
# define PRINTF_TESTS_H

# ifndef FT_PRINTF_H
#  include <ft_printf.h>
# endif

/*
** PRINTF_TESTS
*/

void	test_print_bubble(t_format *bubble);
void	print_bit_tab(int *tab, int len);

/*
** TEST_TOOLS
*/

void	test_return(int ret);
void	pf_putstr_no_sp(char *str);
void	ft_fill(char *str, int start, int len, char c);
void	test_message(char *s, char *color);

#endif
