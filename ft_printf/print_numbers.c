/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_numbers.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/27 23:20:58 by algrele           #+#    #+#             */
/*   Updated: 2018/05/18 16:21:16 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <pf_mini_libft.h>
#include <printf_tests.h>

/*
** get number value with ntoa of argument, with corresponding base.
** send apply_num_options and return length of what was printed.
*/

int	print_number(t_format *bubble, intmax_t argument)
{
	char	*number;
	int		len;

	if (bubble->type == 'o')
		number = ft_untoa_base(argument, 8, 0);
	else if (bubble->type == 'X')
		number = ft_untoa_base(argument, 16, 1);
	else if (bubble->type == 'x')
		number = ft_untoa_base(argument, 16, 0);
	else if (bubble->type == 'u')
		number = ft_untoa_base(argument, 10, 0);
	else
		number = ft_ntoa_base(argument, 10);
	len = apply_num_options(bubble, number);
	return (len);
}

/*
** %p is similar to %x. F_SHARP = 2 helps apply exceptions in number options.
** if pointer is null, act as if it is a %x conversion with 0 as value.
*/

int	print_pointer(t_format *b, void *argument)
{
	if (!argument)
	{
		if (!(argument = pf_strdup("0")))
			return (-1);
		b->type = 'x';
		b->flags[F_SHARP] = 2;
		return (apply_num_options(b, argument));
	}
	b->type = 'x';
	b->flags[F_SHARP] = 2;
	return (print_number(b, (uintmax_t)argument));
}
