/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_tests.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/27 22:26:30 by algrele           #+#    #+#             */
/*   Updated: 2018/05/18 16:16:18 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <printf_tests.h>
#include <ft_printf.h>
#include <pf_mini_libft.h>

void	test_print_bubble(t_format *bubble)
{
	pf_putstr_c("\n> found conversion : %", SUN);
	pf_putchar_c(bubble->type, SUN);
	pf_putstr(PURPLE);
	pf_putstr("\n - with flags (#0-+ ): ");
	{
		pf_putchar('0' + bubble->flags[0]);
		pf_putchar('0' + bubble->flags[1]);
		pf_putchar('0' + bubble->flags[2]);
		pf_putchar('0' + bubble->flags[3]);
		pf_putchar('0' + bubble->flags[4]);
	}
	pf_putstr("\n - with field width : ");
	pf_putnbr(bubble->field);
	pf_putstr("\n - with precision : ");
	pf_putnbr(bubble->preci);
	pf_putstr("\n - with modifiers : ");
	pf_putnbr(bubble->modi);
	pf_putstr(EOC);
	pf_putchar('\n');
}

void	print_bit_tab(int *tab, int len)
{
	int i;

	i = 0;
	pf_putchar('\n');
	while (i < len)
	{
		pf_putnbr(tab[i]);
		if (i == 7 || i == 14 || i == 21)
			pf_putstr(" | ");
		i++;
	}
	pf_putchar('\n');
}
