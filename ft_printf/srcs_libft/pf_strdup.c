/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pf_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 22:03:44 by algrele           #+#    #+#             */
/*   Updated: 2018/05/03 17:51:28 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <pf_mini_libft.h>

char	*pf_strdup(const char *s1)
{
	char	*dup;

	dup = pf_strnew(pf_strlen(s1));
	if (!dup)
		return (NULL);
	dup = pf_strcpy(dup, s1);
	return (dup);
}
