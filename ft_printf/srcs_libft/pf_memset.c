/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pf_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/03 17:48:08 by algrele           #+#    #+#             */
/*   Updated: 2018/05/03 17:51:17 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <pf_mini_libft.h>

void	*pf_memset(void *b, int c, size_t len)
{
	unsigned char *count;

	if (len == 0)
		return (b);
	count = (unsigned char *)b;
	while (len > 0)
	{
		*count = (unsigned char)c;
		count++;
		len--;
	}
	return (b);
}
