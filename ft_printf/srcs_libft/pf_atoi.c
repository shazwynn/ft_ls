/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pf_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 02:43:25 by algrele           #+#    #+#             */
/*   Updated: 2018/05/19 02:43:45 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <pf_mini_libft.h>

int		pf_atoi(char *str)
{
	int	res;
	int	i;
	int	s;

	res = 0;
	i = 0;
	s = 0;
	while (str[s] < 32)
		s++;
	if (str[s] == '+' || str[s] == '-')
		i++;
	while (str[i + s] >= '0' && str[i + s] <= '9')
	{
		res = res * 10 + str[i + s] - '0';
		i++;
	}
	if (str[s] == '-')
		res = -res;
	return (res);
}
