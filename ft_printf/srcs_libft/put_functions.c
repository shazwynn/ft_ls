/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_functions.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/03 17:39:21 by algrele           #+#    #+#             */
/*   Updated: 2018/05/19 02:28:40 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <pf_mini_libft.h>
#include <printf_tests.h>

int		pf_putchar(char c)
{
	write(1, &c, 1);
	return (1);
}

void	pf_putstr(char *s)
{
	int	i;

	i = 0;
	while (s[i])
	{
		pf_putchar(s[i]);
		i++;
	}
}

void	pf_putnbr(int n)
{
	unsigned int	nbr;

	if (n < 0)
	{
		pf_putchar('-');
		nbr = -n;
	}
	else
		nbr = n;
	if (nbr >= 10)
		pf_putnbr(nbr / 10);
	pf_putchar('0' + nbr % 10);
}
