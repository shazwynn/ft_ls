/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_color_tools.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/03 17:37:03 by algrele           #+#    #+#             */
/*   Updated: 2018/05/19 03:37:44 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <pf_mini_libft.h>
#include <printf_tests.h>

void	pf_putchar_c(char c, char *color)
{
	int		x;

	x = pf_atoi(color);
	if (x > 0 && x < 256)
	{
		pf_putstr("\e[38;5;");
		pf_putnbr(x);
		pf_putstr("m");
	}
	else if (color[1] == '[')
		pf_putstr(color);
	pf_putchar(c);
	pf_putstr(EOC);
}

void	pf_putstr_c(char *s, char *color)
{
	int		x;

	x = pf_atoi(color);
	if (x > 0 && x < 256)
	{
		pf_putstr("\e[38;5;");
		pf_putnbr(x);
		pf_putstr("m");
	}
	else if (color[1] == '[')
		pf_putstr(color);
	pf_putstr(s);
	pf_putstr(EOC);
}

void	pf_putnbr_c(int n, char *color)
{
	int		x;

	x = pf_atoi(color);
	if (x > 0 && x < 256)
	{
		pf_putstr("\e[38;5;");
		pf_putnbr(x);
		pf_putstr("m");
	}
	else if (color[1] == '[')
		pf_putstr(color);
	pf_putnbr(n);
	pf_putstr(EOC);
}

void	pf_putendl_c(char *s, char *color)
{
	pf_putstr_c(s, color);
	pf_putchar('\n');
}

/*
** add color and set skip to next part of string format.
*/

void	pf_print_color(int *skip, va_list ap)
{
	char	*color;
	int		x;

	color = va_arg(ap, char *);
	if (!color)
		pf_putstr(EOC);
	else
	{
		x = pf_atoi(color);
		if (x > 0 && x < 256)
		{
			pf_putstr("\e[38;5;");
			pf_putnbr(x);
			pf_putstr("m");
		}
		else if (color[1] == '[')
			pf_putstr(color);
		else
			write(1, EOC, 4);
	}
	*skip = *skip + 3;
}
