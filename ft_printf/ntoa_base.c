/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ntoa_base.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/27 22:38:29 by algrele           #+#    #+#             */
/*   Updated: 2018/05/18 16:20:27 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <pf_mini_libft.h>

static void	rec_ntoa_base(uintmax_t value, uintmax_t base, char *res)
{
	int	i;

	i = 0;
	if (value >= base)
	{
		rec_ntoa_base(value / base, base, res);
		rec_ntoa_base(value % base, base, res);
	}
	if (value < base)
	{
		while (res[i])
			i++;
		if (value <= 9)
			res[i] = value + 48;
		if (value >= 10)
			res[i] = value + 55;
	}
}

/*
** - (maj * 32) to obtain capital letter in case 'X' conversion (maj == 1)
*/

static void	rec_untoa_base(uintmax_t value, uintmax_t base, char *res, int maj)
{
	int	i;

	i = 0;
	if (value >= base)
	{
		rec_untoa_base(value / base, base, res, maj);
		rec_untoa_base(value % base, base, res, maj);
	}
	if (value < base)
	{
		while (res[i])
			i++;
		if (value <= 9)
			res[i] = value + 48;
		if (value >= 10)
			res[i] = value + 87 - (maj * 32);
	}
}

static int	ft_count_ntoa(uintmax_t value, int base)
{
	int	count;

	count = 0;
	if (value == 0)
		count++;
	while (value > 0)
	{
		value = value / base;
		count++;
	}
	return (count);
}

/*
** for signed conversions
*/

char		*ft_ntoa_base(intmax_t value, int base)
{
	char	*res;
	int		count;

	count = ft_count_ntoa(value < 0 ? -value : value, base);
	if (value < 0)
		count++;
	res = pf_strnew(count);
	if (!res || base < 2 || base > 16)
		return (NULL);
	if (value < 0 && base == 10)
	{
		res[0] = '-';
		rec_ntoa_base(-value, base, res);
	}
	else
		rec_ntoa_base(value, base, res);
	return (res);
}

/*
** for unsigned conversions. maj = 1 if hexadecimal is in capital letter form.
*/

char		*ft_untoa_base(uintmax_t value, int base, int maj)
{
	char	*res;
	int		count;

	count = ft_count_ntoa(value, base);
	res = pf_strnew(count);
	if (!res || base < 2 || base > 16)
		return (NULL);
	rec_untoa_base(value, base, res, maj);
	return (res);
}
