/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   apply_type.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/27 23:34:03 by algrele           #+#    #+#             */
/*   Updated: 2018/05/18 18:34:26 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <pf_mini_libft.h>
#include <printf_tests.h>

static int	apply_signed_number(t_format *b, va_list arg)
{
	int	len;

	len = 0;
	if (b->modi == 0)
		len = print_number(b, va_arg(arg, int));
	else if (b->modi == M_L)
		len = print_number(b, va_arg(arg, long));
	else if (b->modi == M_LL)
		len = print_number(b, va_arg(arg, long long));
	else if (b->modi == M_J)
		len = print_number(b, va_arg(arg, intmax_t));
	else if (b->modi == M_Z)
		len = print_number(b, va_arg(arg, ssize_t));
	else if (b->modi == M_H)
		len = print_number(b, (short)va_arg(arg, int));
	else if (b->modi == M_HH)
		len = print_number(b, (signed char)va_arg(arg, int));
	return (len);
}

static int	apply_unsigned_number(t_format *b, va_list arg)
{
	int				len;

	len = 0;
	if (b->modi == 0)
		len = print_number(b, va_arg(arg, unsigned int));
	else if (b->modi == M_L)
		len = print_number(b, va_arg(arg, unsigned long));
	else if (b->modi == M_LL)
		len = print_number(b, va_arg(arg, unsigned long long));
	else if (b->modi == M_J)
		len = print_number(b, va_arg(arg, uintmax_t));
	else if (b->modi == M_Z)
		len = print_number(b, va_arg(arg, size_t));
	else if (b->modi == M_H)
		len = print_number(b, (unsigned short)va_arg(arg, unsigned int));
	else if (b->modi == M_HH)
		len = print_number(b, (unsigned char)va_arg(arg, unsigned int));
	return (len);
}

/*
** retrieve va_list argument with the correct type, and send to corresponding
** print_ functions.
*/

int			apply_type(t_format *b, va_list arg)
{
	if (b->type == '%')
		return (print_char(b, '%'));
	else if (b->type == 'c')
	{
		if (b->modi == M_L)
			return (print_wchar(b, va_arg(arg, wint_t)));
		return (print_char(b, va_arg(arg, int)));
	}
	else if (b->type == 's')
	{
		if (b->modi == M_L)
			return (print_wstring(b, va_arg(arg, wchar_t*)));
		return (print_string(b, va_arg(arg, char*)));
	}
	else if (b->type == 'p')
		return (print_pointer(b, va_arg(arg, void*)));
	else if (b->type == 'd')
		return (apply_signed_number(b, arg));
	else if (c_in_macro(b->type, U_CONVERSIONS))
		return (apply_unsigned_number(b, arg));
	return (0);
}
