/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   number_options.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/28 00:19:22 by algrele           #+#    #+#             */
/*   Updated: 2018/05/18 16:20:43 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <pf_mini_libft.h>
#include <printf_tests.h>

/*
** Adds spacing or plus sign for positive nums (flags ' ' and '+')
** Adds '0', '0x', '0X' as a prefix for 'o', 'p', 'x', 'X' conversions
** checks if value is zero for 'o', 'x', 'X' but not 'p' (F_SHARP == 2)
*/

static char	*sign_me(t_format *b, char *num, int zero)
{
	if (b->flags[F_SHARP] && b->type == 'o' && (!zero || b->preci == 0))
		num = ft_join_head("0", num);
	else if (b->flags[F_SHARP] && b->type == 'x' && !zero)
		num = ft_join_head("0x", num);
	else if (b->flags[F_SHARP] == 2)
		num = ft_join_head("0x", num);
	else if (b->flags[F_SHARP] && b->type == 'X' && !zero)
		num = ft_join_head("0X", num);
	else if (num && num[0] != '-' && b->flags[F_SPACE] && b->type == 'd')
		num = ft_join_head(" ", num);
	else if (num && num[0] != '-' && b->flags[F_PLUS] && b->type == 'd')
		num = ft_join_head("+", num);
	return (num);
}

/*
** applies precision to number, add zeroes between number and '-' sign.
** for an 'o' conversion that already has a starting '0', '#' flag is ignored.
*/

static char	*apply_num_preci(t_format *b, char *num, int zero, int len)
{
	if (b->preci == 0 && zero)
		num[0] = '\0';
	else if (b->preci > 0 && ((num[0] != '-' && len < b->preci) ||
				(num[0] == '-' && len - 1 < b->preci)))
	{
		if (c_in_macro('-', num))
			num = ft_join_n_at('0', num, 1, b->preci - len + 1);
		else
			num = ft_join_n_head('0', num, b->preci - len);
	}
	if (b->type == 'o' && num[0] == '0')
		b->flags[F_SHARP] = 0;
	return (num);
}

/*
** applies field length to number, with corresponding flags.
** if '-' flag -> left adjust with spaces
** if no specific flag -> right adjust with spaces
** if '0' flag -> right adjust with zeroes (ft_join_n_head) or between number
** and sign ('+', ' ', '-', "0[x/X]")
*/

static char	*apply_field(t_format *b, char *num, int len, int zero)
{
	if (b->field && len < b->field)
	{
		if (b->flags[F_ZERO])
		{
			if (b->flags[F_SHARP] && b->type == 'o')
				num = ft_join_n_at('0', num, 1, b->field - len);
			else if (b->flags[F_SHARP] && (b->type == 'X' || b->type == 'x'))
			{
				if (zero && b->flags[F_SHARP] != 2)
					num = ft_join_n_at('0', num, 0, b->field - len);
				else
					num = ft_join_n_at('0', num, 2, b->field - len);
			}
			else if (num[0] == '-' || num[0] == '+' || num[0] == ' ')
				num = ft_join_n_at('0', num, 1, b->field - len);
			else
				num = ft_join_n_head('0', num, b->field - len);
		}
		else if (b->flags[F_MINUS])
			num = ft_join_n_tail(' ', num, b->field - len);
		else
			num = ft_join_n_head(' ', num, b->field - len);
	}
	return (num);
}

/*
** successively apply precision, flags and field length to a number conversion.
** uses various join functions that create new string and free source string.
** prints number string in one go as soon as done.
*/

int			apply_num_options(t_format *b, char *num)
{
	int	len;
	int zero;

	zero = 0;
	if (!num)
		return (-1);
	len = pf_strlen(num);
	if (num[0] == '0' && len == 1)
		zero = 1;
	if (!(num = apply_num_preci(b, num, zero, len)))
		return (-1);
	if (!(num = sign_me(b, num, zero)))
		return (-1);
	if (!(num = apply_field(b, num, pf_strlen(num), zero)))
		return (-1);
	len = pf_strlen(num);
	write(1, &num[0], len);
	free(num);
	return (len);
}
