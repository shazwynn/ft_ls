/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/17 16:16:55 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 22:05:54 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include <unistd.h>
# include <sys/ioctl.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <sys/acl.h>
# include <sys/xattr.h>
# include <dirent.h>
# include <pwd.h>
# include <grp.h>
# include <time.h>
# include <errno.h>
# include "ft_ls_structs.h"
# include "ft_printf.h"
# include "libft.h"

/*
** Various FT_LS Macros
*/

# define MC_MAX_FL		0
# define MC_F_PERLINE	1
# define MC_LINES		2
# define LS_OPTIONS		"1aABbcdFfGgiklnopqRrSsTtuU"

/*
** Flags for options
** # define F_ACL			(1 << 8) // NOT IN USE
** # define F_EXTATT		(1 << 9) // NOT IN USE
** # define F_FLAGS			(1 << 12) // NOT IN USE
** # define F_HUMANVAL		(1 << 13) // NOT IN USE
** # define F_NOTABS	    (1 << 31) // NOT IN USE
** # define F_SORTACROSS	(1 << 25) // NOT IN USE
** # define F_FOLLOWSYM 	// -HLP
*/

# define F_LISTDOT		(1 << 0)
# define F_LONGFORM		(1 << 1)
# define F_RECURSIVE	(1 << 2)
# define F_REVERSESORT	(1 << 3)
# define F_TIMESORT		(1 << 4)
# define F_ACCESSTIME	(1 << 5)
# define F_LISTDIR		(1 << 6)
# define F_NOSORT		(1 << 7)
# define F_GROUPONLY	(1 << 10)
# define F_NOGROUP		(1 << 11)
# define F_INODE		(1 << 14)
# define F_KBLOCKS		(1 << 15)
# define F_NONPRINT		(1 << 16)
# define F_CREATTIME	(1 << 17)
# define F_NUMERICONLY	(1 << 18)
# define F_OCTAL		(1 << 19)
# define F_OCTAL_ESC	(1 << 20)
# define F_SECTIME		(1 << 21)
# define F_SINGLECOL	(1 << 22)
# define F_NBLOCKS		(1 << 23)
# define F_SLASH		(1 << 24)
# define F_STATUSTIME	(1 << 26)
# define F_STREAM		(1 << 27)
# define F_SIZESORT		(1 << 28)
# define F_TYPE			(1 << 29)
# define F_COLORS		(1 << 30)

/*
** DEFINING COLORS BY TYPE
*/

# define C_NONE			"\033[39;49m\033[0m"
# define C_DIR			"\033[1m\033[36m"
# define C_LNK			"\033[35m"
# define C_SOCK			"\033[32m"
# define C_FIFO			"\033[33m"
# define C_EXEC_FILE	"\033[31m"
# define C_EXEC_DIR		"\033[30m\033[43m"
# define C_BLK			"\033[34m\033[46m"
# define C_CHR			"\033[34m\033[43m"

/*
** GET_OPTIONS & OPT_TO_FLAGS
*/
int			get_options(int argc, char **argv, int *count_opt);
int			opt_to_flags(char option, int flags);

/*
** LS_SORT_PARAMS
*/
void		ls_sort_params(char **tab, int size, int count_opt);

/*
** OPS_LIST
*/
t_operands	*create_op_node(char *name, int flags);
void		err_ops(t_operands **head, int n_ops);
t_operands	*get_ops(char **argv, int argc, int flags, int count_opt);

/*
** TRAVERSE
*/
int			traverse_ops(t_operands *ops, int fl, int tw);
t_stuff		*ls_sort_print(t_stuff *head, int flags, int tw, int files);

/*
** CREATE_LIST && CREATE_LIST_FILES && FILL_STRUCT
*/
t_stuff		*add_dir(t_stuff *head, char *path, int flags, t_operands *ops);
t_stuff		*create_list(t_operands *ops, int flags, int files);
int			ls_needs_lstat(int fl);
t_stuff		*add_stuff_node(t_stuff *data, t_stuff *head);
t_stuff		*add_files(t_operands *ops, int fl);
t_stuff		*fill_struct(struct stat *buf, char *name, char *path, int flags);

/*
** LIST_SORT
*/
void		list_sort_reverse(t_stuff **head);
void		list_sort_by(t_stuff **head, long param, int fl);
void		list_sort_alpha(t_stuff **head);

/*
** DISPLAY_ISK_TYPE (inode, n_blocks, type, etc..) & DISPLAY_NP (non printables)
*/
void		print_inode_kblocks(t_stuff *data, int flags, t_padding *p);
void		print_i_kb_no_p(t_stuff *head, t_stuff *data, int flags);
void		print_type_suffix(t_stuff *data, mode_t type, int flags);
void		ls_non_printable(char *name, int flags);

/*
** DISPLAY_NAME
*/
void		print_name(char *name, int flags, char *color);
char		*find_colortype(mode_t mode);
void		pr_name_opt(t_stuff *head, t_stuff *node, int fl, int max_p);
void		pr_name_opt_pad(t_stuff *node, int fl, int max_p);

/*
** DISPLAY_LONG
*/
void		print_long_line(t_stuff *d, int flags, t_padding *p);
void		print_time(t_stuff *data, int flags);

/*
** DISPLAY_TOOLS
*/
void		print_num_pad(int number, int maxl, int extraspaces);
void		print_str_pad(char *str, int maxl, int extraspaces);

/*
** LIST_TOOLS
*/
t_stuff		*get_node(t_stuff **head, int index);
t_stuff		*find_next_node(t_stuff *curr, int skip, int flags);
t_stuff		*find_first_node(t_stuff *curr, int flags);

/*
** DISPLAY_PERMISSIONS
*/
void		print_rights(t_stuff *data);
char		*add_rights_buf(char *buf, int ret, int rnum);
char		add_rights_suffix(t_stuff *data);
char		add_rights_prefix(mode_t type);

/*
** DISPLAY_MC (multi columns) && STRUCT_PADDING
*/
void		pr_data_multicol(t_stuff *head, int flags, int termwidth);
t_padding	*init_pad_struct(void);
t_padding	*fill_pad_struct(t_stuff *s_head, t_padding *p_node, int flags);

/*
** DISPLAY_PR (print list functions)
*/
void		pr_data(t_stuff *head, int fl, int termwidth);
void		pr_long(t_stuff *head, int flags, int files);
void		pr_rec(t_stuff *head, int *count, int flags, int termwidth);
void		pr_longrec(t_stuff *head, int *count, int flags, int files);

/*
** FREE_STUFF
*/
void		exit_free(t_operands *ops, t_stuff *d, char *str, struct stat *buf);
void		free_ops_node(t_operands **ops);
void		free_stuff_node(t_stuff **node);
void		delete_stuff_list(t_stuff **head);
void		delete_ops_list(t_operands **head);

/*
** ERROR_HANDLING
*/
void		error_message_exit(int error_code, char letter);
void		print_error_message(char *name, int error);

/*
** FT_JOINPATH
*/
char		*ft_joinpath(char *s1, char *s2);
char		*ft_strfill(char *s, char c, int size);

/*
** LS_DEBUG
*/
void		print_struct(t_stuff *data);
void		ft_print_params(char **argv, int c);
void		flags_reader(int flags);
void		ops_reader(t_operands *ops);
void		pad_reader(t_padding *p_node);
void		padding_test(void);
void		color_test(void);
void		pr_whole_list_names(t_stuff *head, int flags);

#endif
