/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_stuff.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/26 13:16:51 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 19:51:51 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	exit_free(t_operands *ops, t_stuff *data, char *str, struct stat *buf)
{
	if (ops)
		delete_ops_list(&ops);
	if (data)
		delete_stuff_list(&data);
	if (str)
		ft_strdel(&str);
	if (buf)
	{
		free(buf);
		buf = NULL;
	}
	exit(1);
}

void	free_ops_node(t_operands **ops)
{
	t_operands	*tmp;

	tmp = *ops;
	if (!tmp)
		return ;
	if (tmp->op_name)
		ft_strdel(&tmp->op_name);
}

void	free_stuff_node(t_stuff **node)
{
	t_stuff	*tmp;

	tmp = *node;
	if (!tmp)
		return ;
	if (tmp->name)
		ft_strdel(&tmp->name);
	if (tmp->path)
		ft_strdel(&tmp->path);
	if (tmp->full_path)
		ft_strdel(&tmp->full_path);
	if (tmp->linking_to)
		ft_strdel(&tmp->linking_to);
	if (tmp->user_free)
		ft_strdel(&tmp->user);
	if (tmp->group_free)
		ft_strdel(&tmp->group);
}

void	delete_stuff_list(t_stuff **head)
{
	t_stuff	*tmp;
	t_stuff	*ptr;

	ptr = *head;
	if (!ptr)
		return ;
	while (ptr)
	{
		if (ptr->side_next)
			delete_stuff_list(&(ptr->side_next));
		free_stuff_node(&ptr);
		tmp = ptr->next;
		free(ptr);
		ptr = NULL;
		ptr = tmp;
	}
}

void	delete_ops_list(t_operands **head)
{
	t_operands	*tmp;
	t_operands	*ptr;

	ptr = *head;
	if (!ptr)
		return ;
	while (ptr)
	{
		free_ops_node(&ptr);
		tmp = ptr->next;
		free(ptr);
		ptr = NULL;
		ptr = tmp;
	}
}
