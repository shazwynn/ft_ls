/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_long.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/16 19:19:20 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 16:15:13 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_time(t_stuff *data, int flags)
{
	time_t	f_time;
	time_t	now;
	time_t	six_months;

	f_time = (time_t)data->time;
	if (flags & F_SECTIME)
		ft_printf(" %.20s", ctime(&f_time) + 4);
	else
	{
		six_months = (365 / 2) * 86400;
		now = time(NULL);
		if (f_time > 253402297140)
			ft_printf(" %.7s %.5s", ctime(&f_time) + 4, ctime(&f_time) + 24);
		else if (f_time + six_months > now && f_time < now + six_months)
			ft_printf(" %.12s", ctime(&f_time) + 4);
		else
			ft_printf(" %.7s %.4s", ctime(&f_time) + 4, ctime(&f_time) + 20);
	}
}

void	print_long_line(t_stuff *d, int flags, t_padding *p)
{
	if (!d)
		return ;
	print_inode_kblocks(d, flags, p);
	print_rights(d);
	print_num_pad(d->links, p->maxl_links, 1);
	if (!(flags & F_GROUPONLY))
		print_str_pad(d->user, p->maxl_user, 2);
	if (!(flags & F_NOGROUP))
		print_str_pad(d->group, p->maxl_group, 2);
	if (flags & F_GROUPONLY && flags & F_NOGROUP)
		ft_printf("  ");
	if (S_ISBLK(d->mode) || S_ISCHR(d->mode))
		ft_printf(" %2d, %3d", d->devid >> 24 & 0xff, d->devid & 0xffffff);
	else
		print_num_pad(d->size, p->maxl_size, 0);
	print_time(d, flags);
	ft_printf(" ");
	pr_name_opt(d, d, flags, -1);
	if (d->is_symlink)
	{
		ft_printf(" -> ");
		print_name(d->linking_to, flags, NULL);
		ft_printf("\n");
	}
}
