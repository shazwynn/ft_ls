/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_options.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/22 12:46:42 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 16:15:13 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int	get_options(int argc, char **argv, int *count_opt)
{
	int	i;
	int	j;
	int	let;
	int	flags;

	i = 1;
	flags = 0;
	while (i < argc)
	{
		if (argv[i][0] != '-' || !argv[i][1])
			return (flags);
		if (argv[i][1] == '-' && (*count_opt)++)
			return (flags);
		j = 1;
		while (argv[i][j])
		{
			if ((let = ft_i_in_macro(argv[i][j], LS_OPTIONS)) < 0)
				error_message_exit(1, argv[i][j]);
			flags = opt_to_flags(argv[i][j], flags);
			j++;
		}
		i++;
		(*count_opt)++;
	}
	return (flags);
}
