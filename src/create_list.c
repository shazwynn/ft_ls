/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_list.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/15 11:49:54 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 22:21:16 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static t_stuff	*add_dir_lstat(char *tmp, char *name, char *path,
		int flags)
{
	struct stat *buf;
	t_stuff		*data;

	if (!(buf = ft_memalloc(sizeof(struct stat))))
		return (NULL);
	ft_bzero(buf, sizeof(struct stat));
	if (lstat(tmp, buf) == -1)
	{
		print_error_message(tmp, errno);
		free(buf);
		buf = NULL;
		return (NULL);
	}
	data = fill_struct(buf, name, path, flags);
	free(buf);
	buf = NULL;
	return (data);
}

static t_stuff	*add_side_dirs(t_stuff *h, DIR *dir, int fl,
		t_operands *ops)
{
	t_stuff	*p;

	p = h;
	while (p)
	{
		if (fl & F_RECURSIVE && p->is_symlink == 0)
		{
			dir = opendir(p->full_path);
			if (!dir && errno != ENOTDIR && errno != ELOOP)
			{
				if (p->name[0] != '.' || fl & F_LISTDOT)
					p->ret = errno;
			}
			else if (dir && ft_strcmp(p->name, ".") && ft_strcmp(p->name, ".."))
			{
				closedir(dir);
				if (p->name[0] != '.' || fl & F_LISTDOT)
					p->side_next = add_dir(p->side_next, p->full_path, fl, ops);
			}
			else if (dir)
				closedir(dir);
		}
		p = p->next;
	}
	return (h);
}

t_stuff			*add_dir(t_stuff *head, char *path, int flags, t_operands *ops)
{
	t_stuff			*data;
	struct dirent	*dir_struct;
	DIR				*curr_dir;
	char			*tmp;

	dir_struct = NULL;
	errno = 0;
	curr_dir = opendir(path);
	while ((dir_struct = readdir(curr_dir)))
	{
		if (!(tmp = ft_joinpath(path, dir_struct->d_name)))
			exit_free(ops, head, NULL, NULL);
		if (ft_strcmp(tmp, ".") && ft_strcmp(tmp, ".."))
		{
			if (!(data = add_dir_lstat(tmp, dir_struct->d_name, path, flags)))
				exit_free(ops, head, tmp, NULL);
			ft_bzero(tmp, ft_strlen(tmp));
			head = add_stuff_node(data, head);
		}
		ft_strdel(&tmp);
	}
	closedir(curr_dir);
	head = add_side_dirs(head, curr_dir, flags, ops);
	return (head);
}

t_stuff			*add_error_dir(t_stuff *head, char *path, int flags, int error)
{
	t_stuff *data;
	t_stuff *prev;

	prev = NULL;
	if (ft_strcmp(path, ".") && ft_strcmp(path, ".."))
	{
		data = fill_struct(NULL, path, path, flags);
		if (data)
			data->ret = error;
		if (prev)
			prev->next = data;
		prev = data;
		if (!head)
			head = data;
	}
	return (head);
}

t_stuff			*create_list(t_operands *ops, int flags, int files)
{
	t_stuff		*list;
	t_operands	*tmp;

	list = NULL;
	tmp = ops;
	if (files == 0 && (tmp->skip == 2 || tmp->skip == -3))
	{
		if (tmp->curr_dir)
			list = add_dir(list, tmp->op_name, flags, ops);
		else if (tmp->skip == -3)
			list = add_error_dir(list, tmp->op_name, flags, EACCES);
		tmp->skip = -2;
	}
	if (files == 1)
		list = add_files(tmp, flags);
	return (list);
}
