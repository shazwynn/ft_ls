/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_isk_type.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/16 19:17:12 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 16:15:13 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_type_suffix(t_stuff *data, mode_t type, int flags)
{
	if (type == S_IFDIR)
		ft_printf("/");
	else if (flags & F_SLASH)
	{
		if (!(flags & F_SINGLECOL))
			ft_printf(" ");
		return ;
	}
	else if (type == S_IFLNK)
		ft_printf("@");
	else if (type == S_IFSOCK)
		ft_printf("=");
	else if (type == S_IFIFO)
		ft_printf("|");
	else if (type == S_IFWHT)
		ft_printf("%");
	else if (data->mode & (S_IXUSR | S_IXGRP | S_IXOTH))
		ft_printf("*");
	else if (!(flags & F_SINGLECOL))
		ft_printf(" ");
}

void	print_inode_kblocks(t_stuff *data, int flags, t_padding *p)
{
	if (flags & F_INODE)
		print_num_pad(data->inode, p->maxl_inode, 1);
	if (flags & F_NBLOCKS)
	{
		if (flags & F_KBLOCKS)
			print_num_pad(data->n_blocks / 2, p->maxl_n_blocks, 1);
		else
			print_num_pad(data->n_blocks, p->maxl_n_blocks, 1);
	}
}

void	print_i_kb_no_p(t_stuff *head, t_stuff *data, int flags)
{
	t_padding	*p;

	p = init_pad_struct();
	p = fill_pad_struct(head, p, flags);
	print_inode_kblocks(data, flags, p);
	free(p);
	p = NULL;
}
