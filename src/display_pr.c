/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_pr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/16 18:58:13 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 21:53:47 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int	c_total(t_stuff *head, int flags)
{
	int count;

	count = 0;
	while (head)
	{
		if (head->name && (head->name[0] != '.' || flags & F_LISTDOT))
			count = count + head->n_blocks;
		head = head->next;
	}
	return (count);
}

void		pr_data(t_stuff *head, int fl, int termwidth)
{
	t_stuff	*curr;

	if (head->ret)
	{
		print_error_message(head->name, head->ret);
		head->ret = 0;
		return ;
	}
	if (fl & F_NBLOCKS && find_first_node(head, fl) && head->next)
		ft_printf("total %d\n", fl & F_KBLOCKS ? c_total(head, fl) / 2 :
				c_total(head, fl));
	if (fl & F_SINGLECOL)
	{
		curr = head;
		while (curr)
		{
			pr_name_opt(head, curr, fl, -1);
			curr = curr->next;
		}
	}
	else
		pr_data_multicol(head, fl, termwidth);
}

void		pr_long(t_stuff *head, int flags, int files)
{
	t_padding	*p_node;

	if (head->ret)
	{
		print_error_message(head->name, head->ret);
		head->ret = 0;
		return ;
	}
	if (!(p_node = init_pad_struct()))
		return ;
	p_node = fill_pad_struct(head, p_node, flags);
	if (find_first_node(head, flags) && head->next && !files)
		ft_printf("total %d\n", c_total(head, flags));
	while (head)
	{
		if (head->name && (head->name[0] != '.' || flags & F_LISTDOT))
			print_long_line(head, flags, p_node);
		head = head->next;
	}
	free(p_node);
	p_node = NULL;
}

void		pr_rec(t_stuff *head, int *count, int flags, int termwidth)
{
	t_stuff *tmp;

	if (*count != 0)
		ft_printf("%s:\n", head->path);
	pr_data(head, flags, termwidth);
	*count = *count + 1;
	tmp = head;
	while (tmp)
	{
		if (tmp->ret)
		{
			ft_printf("\n%s:\n", tmp->full_path ? tmp->full_path : tmp->name);
			print_error_message(tmp->name, tmp->ret);
			tmp->ret = 0;
		}
		if (tmp->side_next)
		{
			ft_printf("\n");
			pr_rec(tmp->side_next, count, flags, termwidth);
		}
		tmp = tmp->next;
	}
}

void		pr_longrec(t_stuff *head, int *count, int flags, int files)
{
	t_stuff *tmp;

	if (*count != 0)
		ft_printf("%s:\n", head->path);
	pr_long(head, flags, files);
	*count = *count + 1;
	tmp = head;
	while (tmp)
	{
		if (tmp->ret)
		{
			ft_printf("\n%s:\n", tmp->full_path ? tmp->full_path : tmp->name);
			print_error_message(tmp->name, tmp->ret);
			tmp->ret = 0;
		}
		if (tmp->side_next)
		{
			ft_printf("\n");
			pr_longrec(tmp->side_next, count, flags, files);
		}
		tmp = tmp->next;
	}
}
