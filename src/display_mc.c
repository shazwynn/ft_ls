/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_mc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/16 19:00:21 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 16:15:13 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_ls.h>

static int	find_max_filename(t_stuff *curr, int flags)
{
	size_t	max_size;
	size_t	len;
	t_stuff	*tmp;

	max_size = 0;
	tmp = curr;
	while (tmp)
	{
		if (tmp->name && (!(tmp->name[0] == '.') || flags & F_LISTDOT))
			if ((len = ft_strlen(tmp->name)) > max_size)
				max_size = len;
		tmp = tmp->next;
	}
	if (flags & F_SLASH || flags & F_TYPE)
		max_size++;
	return (max_size + 1);
}

static int	find_n_lines(t_stuff *curr, int skip, int flags)
{
	int		files;
	t_stuff	*tmp;

	tmp = curr;
	files = 0;
	while (tmp)
	{
		if (tmp->name && (!(tmp->name[0] == '.') || flags & F_LISTDOT))
			files++;
		tmp = tmp->next;
	}
	if (files % skip == 0)
		return (files / skip);
	return ((files / skip) + 1);
}

static void	fill_mc_tab(t_stuff *curr, int flags, int termwidth, int *tab)
{
	tab[MC_MAX_FL] = find_max_filename(curr, flags);
	tab[MC_F_PERLINE] = termwidth / tab[MC_MAX_FL];
	tab[MC_F_PERLINE] = tab[MC_F_PERLINE] < 1 ? 1 : tab[MC_F_PERLINE];
	tab[MC_LINES] = find_n_lines(curr, tab[MC_F_PERLINE], flags);
	tab[MC_LINES] = tab[MC_LINES] == 0 ? 1 : tab[MC_LINES];
}

void		pr_data_multicol(t_stuff *head, int flags, int termwidth)
{
	int		i;
	int		j;
	int		tab[3];
	t_stuff	*curr;
	t_stuff	*tmp;

	curr = head;
	curr = find_first_node(curr, flags);
	fill_mc_tab(curr, flags, termwidth, tab);
	i = 0;
	while (curr && i < tab[MC_LINES] && ++i)
	{
		curr = find_first_node(curr, flags);
		pr_name_opt(head, curr, flags, tab[MC_MAX_FL]);
		tmp = curr;
		j = 1;
		while (tmp && j < tab[MC_F_PERLINE] && j++)
		{
			tmp = find_next_node(tmp, tab[MC_LINES] + 1, flags);
			if (tmp)
				pr_name_opt(head, tmp, flags, tab[MC_MAX_FL]);
		}
		ft_printf("\n");
		curr = curr->next;
	}
}
