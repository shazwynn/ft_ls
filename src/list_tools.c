/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/17 19:48:27 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 19:53:55 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_stuff	*get_node(t_stuff **head, int index)
{
	t_stuff *node;

	node = *head;
	while (index > 0 && node)
	{
		node = node->next;
		index--;
	}
	return (node);
}

t_stuff	*find_next_node(t_stuff *curr, int skip, int flags)
{
	t_stuff	*tmp;

	tmp = curr;
	while (tmp && tmp->name && tmp->name[0] == '.' && !(flags & F_LISTDOT))
		tmp = tmp->next;
	while (tmp && skip > 1)
	{
		if (tmp->name && (tmp->name[0] != '.' || flags & F_LISTDOT))
			skip--;
		tmp = tmp->next;
	}
	while (tmp && tmp->name && tmp->name[0] == '.' && !(flags & F_LISTDOT))
		tmp = tmp->next;
	return (tmp);
}

t_stuff	*find_first_node(t_stuff *curr, int flags)
{
	t_stuff	*tmp;

	tmp = curr;
	if (flags & F_LISTDOT)
		return (curr);
	while (tmp && tmp->name && tmp->name[0] == '.')
		tmp = tmp->next;
	return (tmp);
}
