/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_tools.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/16 19:22:08 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 19:51:25 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_num_pad(int number, int maxl, int extraspaces)
{
	int num_size;

	num_size = ft_c_digits(number);
	while (maxl-- > num_size)
		ft_printf(" ");
	ft_printf("%d", number);
	while (extraspaces--)
		ft_printf(" ");
}

void	print_str_pad(char *str, int maxl, int extraspaces)
{
	int	len;

	len = (int)ft_strlen(str);
	ft_printf("%s", str);
	while (maxl-- > len)
		ft_printf(" ");
	while (extraspaces--)
		ft_printf(" ");
}
