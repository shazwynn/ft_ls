/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct_tools.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/15 11:49:38 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 21:59:16 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static t_stuff	*init_struct(void)
{
	t_stuff	*data;

	if (!(data = ft_memalloc(sizeof(t_stuff))))
		return (NULL);
	data->name = NULL;
	data->path = NULL;
	data->full_path = NULL;
	data->is_symlink = 0;
	data->linking_to = NULL;
	data->user = NULL;
	data->group = NULL;
	data->next = NULL;
	data->side_next = NULL;
	data->user_free = 0;
	data->group_free = 0;
	return (data);
}

static int		symlink_check(t_stuff *data)
{
	char	*buf2;
	int		ret;

	if (!(S_ISLNK(data->mode)) || !(buf2 = ft_memalloc(data->size + 1)))
		return (0);
	if ((ret = readlink(data->full_path, buf2, data->size + 1)) == -1)
	{
		ft_strdel(&buf2);
		return (0);
	}
	data->linking_to = ft_strdup(buf2);
	ft_strdel(&buf2);
	return (1);
}

static t_stuff	*add_id_and_group(t_stuff *data, struct stat *buf, int flags)
{
	struct passwd	*buf_passwd;
	struct group	*buf_group;

	if (!(flags & F_GROUPONLY))
	{
		if (flags & F_NUMERICONLY || !(buf_passwd = getpwuid(buf->st_uid)))
		{
			data->user = ft_itoa(buf->st_uid);
			data->user_free = 1;
		}
		else
			data->user = buf_passwd->pw_name;
	}
	if (!(flags & F_NOGROUP))
	{
		if (flags & F_NUMERICONLY || !(buf_group = getgrgid(buf->st_gid)))
		{
			data->group = ft_itoa(buf->st_gid);
			data->group_free = 1;
		}
		else
			data->group = buf_group->gr_name;
	}
	return (data);
}

static t_stuff	*add_time(t_stuff *data, struct stat *buf, int flags)
{
	data->ntime = 0;
	data->time = 0;
	if (flags & F_ACCESSTIME)
	{
		data->time = buf->st_atime;
		data->ntime = buf->st_atimespec.tv_nsec;
	}
	else if (flags & F_STATUSTIME)
	{
		data->time = buf->st_ctime;
		data->ntime = buf->st_ctimespec.tv_nsec;
	}
	else if (flags & F_CREATTIME)
	{
		data->time = buf->st_birthtime;
		data->ntime = buf->st_birthtimespec.tv_nsec;
	}
	else
	{
		data->time = buf->st_mtime;
		data->ntime = buf->st_mtimespec.tv_nsec;
	}
	return (data);
}

t_stuff			*fill_struct(struct stat *buf, char *filename, char *path,
		int flags)
{
	t_stuff	*data;

	data = init_struct();
	data->name = ft_strdup(filename);
	data->ret = 0;
	if (!buf)
		return (data);
	add_id_and_group(data, buf, flags);
	data->inode = buf->st_ino;
	data->n_blocks = buf->st_blocks;
	data->links = buf->st_nlink;
	data->size = buf->st_size;
	data->mode = buf->st_mode;
	data->devid = buf->st_rdev;
	if (path && flags & F_RECURSIVE)
		data->path = ft_strdup(path);
	data->full_path = ft_joinpath(path ? path : ".\0", data->name);
	data->is_symlink = symlink_check(data);
	add_time(data, buf, flags);
	return (data);
}
