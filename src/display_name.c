/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_name.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/16 19:21:03 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 16:15:13 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	print_name(char *name, int flags, char *color)
{
	if (color)
		ft_printf("%s", color);
	if (flags & F_NONPRINT)
		ls_non_printable(name, flags);
	else if (flags & F_OCTAL)
		ls_non_printable(name, flags);
	else if (flags & F_OCTAL_ESC)
		ls_non_printable(name, flags);
	else
		ft_printf("%s", name);
	if (color)
		ft_printf("%s", NONE);
}

char	*find_colortype(mode_t mode)
{
	char	*color;

	color = C_NONE;
	if (S_ISDIR(mode))
		color = (mode & S_IWOTH) ? C_EXEC_DIR : C_DIR;
	else if (S_ISLNK(mode))
		color = C_LNK;
	else if (S_ISREG(mode) && (mode & (S_IXUSR | S_IXGRP | S_IXOTH)))
		color = C_EXEC_FILE;
	else if (S_ISSOCK(mode))
		color = C_SOCK;
	else if (S_ISFIFO(mode))
		color = C_FIFO;
	else if (S_ISBLK(mode))
		color = C_BLK;
	else if (S_ISCHR(mode))
		color = C_CHR;
	return (color);
}

void	pr_name_opt(t_stuff *head, t_stuff *node, int fl, int max_p)
{
	char	*color;

	color = NULL;
	if (!(fl & F_LISTDOT) && node->name[0] == '.')
		return ;
	if (!(fl & F_LONGFORM) && (fl & F_INODE || fl & F_NBLOCKS))
		print_i_kb_no_p(head, node, fl);
	if (fl & F_COLORS)
	{
		color = find_colortype(node->mode);
		if (fl & F_LISTDOT || node->name[0] != '.')
			print_name(node->name, fl, color);
	}
	else
		print_name(node->name, fl, NULL);
	if (fl & F_SLASH || fl & F_TYPE)
	{
		print_type_suffix(node, node->mode & S_IFMT, fl);
		max_p--;
	}
	pr_name_opt_pad(node, fl, max_p);
}

void	pr_name_opt_pad(t_stuff *node, int fl, int max_p)
{
	int		extra_spaces;

	if (fl & F_LONGFORM && (node->mode & S_IFMT) == S_IFLNK)
		return ;
	else if (fl & F_LISTDOT || node->name[0] != '.')
	{
		if (fl & F_LONGFORM || fl & F_SINGLECOL)
			ft_printf("\n");
		else if (max_p != -1)
		{
			extra_spaces = max_p - ft_strlen(node->name);
			while (extra_spaces--)
				ft_printf(" ");
		}
		else
			ft_printf("  ");
	}
}
