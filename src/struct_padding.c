/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct_padding.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/04 17:20:16 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 19:56:53 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_padding			*init_pad_struct(void)
{
	t_padding	*p_node;

	if (!(p_node = ft_memalloc(sizeof(t_padding))))
		return (NULL);
	return (p_node);
}

static t_padding	*count_pad_str(t_stuff *s_head, t_padding *p_node,
		int flags)
{
	t_stuff	*tmp;
	size_t	n;
	size_t	u;
	size_t	g;

	tmp = s_head;
	n = 0;
	u = 0;
	g = 0;
	while (tmp)
	{
		if (tmp->name && (tmp->name[0] != '.' || flags & F_LISTDOT))
		{
			n = ft_strlen(tmp->name) > n ? ft_strlen(tmp->name) : n;
			if (!(flags & F_GROUPONLY))
				u = ft_strlen(tmp->user) > u ? ft_strlen(tmp->user) : u;
			if (!(flags & F_NOGROUP))
				g = ft_strlen(tmp->group) > g ? ft_strlen(tmp->group) : g;
		}
		tmp = tmp->next;
	}
	p_node->maxl_name = n;
	p_node->maxl_user = u;
	p_node->maxl_group = g;
	return (p_node);
}

/*
** no need to use a smaller size for blocks with option k because
** real ls doesn't
*/

static t_padding	*count_pad_ib(t_stuff *s_head, t_padding *p_node,
		int flags)
{
	t_stuff	*p;
	int		i;
	int		b;

	p = s_head;
	i = 0;
	b = 0;
	while (p)
	{
		if (p->name && (p->name[0] != '.' || flags & F_LISTDOT))
		{
			i = ft_c_digits(p->inode) > i ? ft_c_digits(p->inode) : i;
			b = ft_c_digits(p->n_blocks) > b ? ft_c_digits(p->n_blocks) : b;
		}
		p = p->next;
	}
	p_node->maxl_inode = i;
	p_node->maxl_n_blocks = b;
	return (p_node);
}

static t_padding	*count_pad_sl(t_stuff *s_head, t_padding *p_node,
		int flags)
{
	t_stuff	*p;
	int		s;
	int		l;

	l = 0;
	s = 0;
	p = s_head;
	while (p)
	{
		if (p->name && (p->name[0] != '.' || flags & F_LISTDOT))
		{
			l = ft_c_digits(p->links) > l ? ft_c_digits(p->links) : l;
			s = ft_c_digits(p->size) > s ? ft_c_digits(p->size) : s;
		}
		p = p->next;
	}
	p_node->maxl_size = s;
	p_node->maxl_links = l;
	return (p_node);
}

t_padding			*fill_pad_struct(t_stuff *s_head, t_padding *p_node,
		int flags)
{
	if (!p_node)
		return (NULL);
	p_node = count_pad_str(s_head, p_node, flags);
	p_node = count_pad_ib(s_head, p_node, flags);
	p_node = count_pad_sl(s_head, p_node, flags);
	return (p_node);
}
