/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_joinpath.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/15 11:48:12 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 16:15:13 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char	*ft_joinpath(char *s1, char *s2)
{
	char	*join;

	if (!s1 || !s2)
		return (NULL);
	if (!(join = ft_strnew(ft_strlen(s1) + ft_strlen(s2) + 1)))
		return (NULL);
	if (s2[0] == '/')
		ft_strcpy(join, s2);
	else
	{
		ft_strcpy(join, s1);
		join[ft_strlen(s1)] = '/';
		ft_strcpy(join + ft_strlen(s1) + 1, s2);
	}
	return (join);
}

/*
** TEST ME
*/

char	*ft_strfill(char *s, char c, int size)
{
	int	i;

	if (!s)
		return (s);
	i = 0;
	while (i < size)
		s[i++] = c;
	return (s);
}
