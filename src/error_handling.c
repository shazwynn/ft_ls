/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_handling.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/15 11:49:59 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 16:15:13 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	error_message_exit(int error_code, char letter)
{
	char	*str;
	char	*usage_start;
	char	*usage_end;
	char	error[78];

	if (error_code == 1)
	{
		str = "ls: illegal option --  \n";
		usage_start = "usage: ls [-";
		usage_end = "] [file ...]\n";
		ft_strcat(error, str);
		error[22] = letter;
		ft_strcat(error, usage_start);
		ft_strcat(error, LS_OPTIONS);
		ft_strcat(error, usage_end);
		ft_putstr_fd(error, 2);
	}
	exit(1);
}

void	print_error_message(char *name, int error)
{
	errno = error;
	ft_putstr_fd("ls: ", 2);
	perror(name);
}
