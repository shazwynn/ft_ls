/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_permissions.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 13:52:14 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 19:53:39 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

/*
** if (S_ISDIR(data->mode)) is another syntax
*/

char	add_rights_prefix(mode_t type)
{
	if (type == S_IFDIR)
		return ('d');
	else if (type == S_IFLNK)
		return ('l');
	else if (type == S_IFREG)
		return ('-');
	else if (type == S_IFBLK)
		return ('b');
	else if (type == S_IFCHR)
		return ('c');
	else if (type == S_IFSOCK)
		return ('s');
	else if (type == S_IFIFO)
		return ('p');
	else
		return ('?');
}

/*
** see strmode.c
*/

void	print_rights(t_stuff *data)
{
	char	buf[12];

	buf[0] = add_rights_prefix(data->mode & S_IFMT);
	buf[1] = (data->mode & S_IRUSR) ? 'r' : '-';
	buf[2] = (data->mode & S_IWUSR) ? 'w' : '-';
	buf[3] = (data->mode & S_IXUSR) ? 'x' : '-';
	buf[4] = (data->mode & S_IRGRP) ? 'r' : '-';
	buf[5] = (data->mode & S_IWGRP) ? 'w' : '-';
	buf[6] = (data->mode & S_IXGRP) ? 'x' : '-';
	buf[7] = (data->mode & S_IROTH) ? 'r' : '-';
	buf[8] = (data->mode & S_IWOTH) ? 'w' : '-';
	buf[9] = (data->mode & S_IXOTH) ? 'x' : '-';
	if (data->mode & S_ISUID)
		buf[3] = buf[3] == '-' ? 'S' : 's';
	if (data->mode & S_ISGID)
		buf[6] = buf[6] == '-' ? 'S' : 's';
	if (data->mode & S_ISVTX)
		buf[9] = buf[9] == '-' ? 'T' : 't';
	buf[10] = add_rights_suffix(data);
	buf[11] = ' ';
	write(1, &buf, 12);
}

/*
** BEWARE potential error returns and FREEing
*/

char	add_rights_suffix(t_stuff *data)
{
	char		*namebuf;
	int			ret;
	acl_t		acl;
	acl_entry_t	acl_entry;

	acl = NULL;
	acl_entry = NULL;
	namebuf = NULL;
	if ((ret = (listxattr(data->full_path, namebuf, 0, XATTR_NOFOLLOW))) > 0)
		return ('@');
	else
	{
		acl = acl_get_link_np(data->full_path, ACL_TYPE_EXTENDED);
		if (acl && acl_get_entry(acl, ACL_FIRST_ENTRY, &acl_entry) != -1)
			return ('+');
		else
		{
			acl_free(acl);
			acl = NULL;
		}
	}
	return (' ');
}
