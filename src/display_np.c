/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_np.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/16 19:09:03 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 16:15:13 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static void	ls_put_octal_esc(char *name, int i)
{
	if (name[i] == '\\')
		ft_putchar('\\');
	else if (name[i] == '\"')
		ft_putchar('"');
	else if (name[i] == '\a')
		ft_putchar('a');
	else if (name[i] == '\b')
		ft_putchar('b');
	else if (name[i] == '\f')
		ft_putchar('f');
	else if (name[i] == '\n')
		ft_putchar('n');
	else if (name[i] == '\r')
		ft_putchar('r');
	else if (name[i] == '\t')
		ft_putchar('t');
	else if (name[i] == '\v')
		ft_putchar('v');
	else
	{
		ft_putchar('0' + (name[i] >> 6));
		ft_putchar('0' + ((name[i] >> 3) & 7));
		ft_putchar('0' + (name[i] & 7));
	}
}

void		ls_non_printable(char *name, int flags)
{
	int	i;

	i = 0;
	while (name && name[i])
	{
		if (ft_isprint(name[i]) && name[i] != '\"' && name[i] != '\\')
			write(1, &name[i], 1);
		else if (flags & F_NONPRINT)
			write(0, "?", 1);
		else if (flags & F_OCTAL_ESC)
		{
			ft_putchar('\\');
			ls_put_octal_esc(name, i);
		}
		else if (flags & F_OCTAL)
		{
			ft_putchar('\\');
			ft_putchar('0' + (name[i] >> 6));
			ft_putchar('0' + ((name[i] >> 3) & 7));
			ft_putchar('0' + (name[i] & 7));
		}
		i++;
	}
}
