/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   traverse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/16 17:51:03 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 21:50:25 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int	is_next_dir(t_operands *ops)
{
	t_operands	*tmp;

	tmp = ops;
	while (tmp)
	{
		if (tmp->skip == 2 || tmp->skip == -2 || tmp->skip == -3)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}

static int	is_multiple(t_operands *ops)
{
	t_operands	*tmp;
	int			i;

	tmp = ops;
	i = 0;
	if (tmp && tmp->next)
		return (1);
	return (0);
}

t_stuff		*ls_sort_print(t_stuff *head, int fl, int tw, int files)
{
	int	count;
	int	lf;

	count = 0;
	lf = fl & F_LONGFORM;
	if (!(fl & F_NOSORT) && head && head->next)
	{
		list_sort_alpha(&head);
		if (!head)
			return (NULL);
		if (fl & F_SIZESORT)
			list_sort_by(&head, 0, fl);
		else if (fl & F_TIMESORT)
			list_sort_by(&head, 1, fl);
		if (fl & F_REVERSESORT)
			list_sort_reverse(&head);
	}
	if (fl & F_RECURSIVE && head)
		lf ? pr_longrec(head, &count, fl, files) : pr_rec(head, &count, fl, tw);
	else if (head)
		lf ? pr_long(head, fl, files) : pr_data(head, fl, tw);
	return (head);
}

int			traverse_ops(t_operands *ops, int fl, int tw)
{
	t_stuff		*head;
	t_operands	*tmp;

	head = NULL;
	if ((head = create_list(ops, fl, 1)) && ls_sort_print(head, fl, tw, 1))
	{
		delete_stuff_list(&head);
		if (is_next_dir(ops))
			ft_printf("\n");
	}
	tmp = ops;
	while (tmp)
	{
		head = create_list(tmp, fl, 0);
		if (tmp->skip == -2 && is_multiple(ops))
			ft_printf("%s:\n", tmp->op_name);
		if ((head = ls_sort_print(head, fl, tw, 0)))
			delete_stuff_list(&head);
		if (tmp->skip == -2 && is_next_dir(tmp->next))
			ft_printf("\n");
		tmp = tmp->next;
	}
	return (0);
}
