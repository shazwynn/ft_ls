/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ops_list.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/15 11:49:04 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 20:55:16 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

/*
** ENOENT = no such file or directory
** EACESS = permission denied
** EINVAL if file not symlink
*/

static void		add_op_skip(t_operands *node, int fl, int open, int link)
{
	if (!(node->curr_dir))
	{
		if (open == EACCES && !(fl & F_LISTDIR))
			node->skip = -3;
		else if (link)
			node->skip = 1;
		else if (open == ENOENT && !link)
		{
			print_error_message(node->op_name, open);
			node->skip = 0;
		}
		else if (open == ENOTDIR || fl & F_LISTDIR)
			node->skip = 1;
		else
			print_error_message(node->op_name, open);
		return ;
	}
	node->skip = 2;
	if ((fl & F_LONGFORM || fl & F_INODE) && link)
		node->skip = 1;
	if (fl & F_LISTDIR)
		node->skip = 1;
}

t_operands		*create_op_node(char *name, int flags)
{
	t_operands	*node;
	int			open;
	ssize_t		link;

	node = ft_memalloc(sizeof(t_operands));
	node->op_name = ft_strdup(name);
	errno = 0;
	node->curr_dir = opendir(name);
	open = errno;
	link = readlink(name, NULL, 0);
	node->skip = 0;
	add_op_skip(node, flags, open, link == -1 ? 0 : 1);
	node->next = NULL;
	if (node->curr_dir)
		closedir(node->curr_dir);
	return (node);
}

void			err_ops(t_operands **head, int n_ops)
{
	t_operands	*tmp;

	tmp = *head;
	while (tmp)
	{
		if (tmp->skip == -3 && n_ops == 2)
		{
			print_error_message(tmp->op_name, EACCES);
			tmp->skip = 0;
		}
		tmp = tmp->next;
	}
}

t_operands		*get_ops(char **argv, int argc, int flags, int count_opt)
{
	t_operands	*head;
	t_operands	*tmp;

	head = NULL;
	if (argc == 1 || count_opt + 1 == argc)
		return (create_op_node(".\0", flags));
	while (count_opt + 1 < argc)
	{
		if (argv[count_opt + 1] && !(argv[count_opt + 1][0]))
		{
			print_error_message("fts_open", ENOENT);
			exit_free(head, NULL, NULL, NULL);
		}
		if (!head)
			head = create_op_node(argv[count_opt + 1], flags);
		else
		{
			tmp = head;
			while (tmp && tmp->next)
				tmp = tmp->next;
			tmp->next = create_op_node(argv[count_opt + 1], flags);
		}
		count_opt++;
	}
	return (head);
}
