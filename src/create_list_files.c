/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_list_files.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/15 23:09:31 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 20:53:30 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

t_stuff			*add_stuff_node(t_stuff *data, t_stuff *head)
{
	t_stuff	*tmp;

	if (!head)
		return (data);
	tmp = head;
	while (tmp && tmp->next)
		tmp = tmp->next;
	tmp->next = data;
	return (head);
}

static t_stuff	*add_file_lstat(t_operands *ops, t_operands *tmp,
		t_stuff *head, int fl)
{
	struct stat	*buf;
	t_stuff		*data;

	data = NULL;
	if (!(buf = ft_memalloc(sizeof(struct stat))))
		return (NULL);
	ft_bzero(buf, sizeof(struct stat));
	if (lstat(tmp->op_name, buf) == -1)
	{
		print_error_message(tmp->op_name, errno);
		exit_free(ops, head, NULL, buf);
	}
	data = fill_struct(buf, tmp->op_name, NULL, fl);
	free(buf);
	buf = NULL;
	return (data);
}

int				ls_needs_lstat(int fl)
{
	if (fl & F_LONGFORM || fl & F_COLORS)
		return (1);
	if (fl & F_INODE || fl & F_NBLOCKS)
		return (1);
	return (0);
}

t_stuff			*add_files(t_operands *ops, int fl)
{
	t_stuff		*data;
	t_stuff		*head;
	t_operands	*tmp;

	data = NULL;
	head = NULL;
	tmp = ops;
	while (tmp)
	{
		if (tmp->skip == 1)
		{
			if (ls_needs_lstat(fl))
				data = add_file_lstat(ops, tmp, head, fl);
			else
				data = fill_struct(NULL, tmp->op_name, NULL, fl);
			head = add_stuff_node(data, head);
			tmp->skip = -1;
		}
		tmp = tmp->next;
	}
	return (head);
}
