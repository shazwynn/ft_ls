/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/15 11:49:00 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 20:33:41 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int	ls_col_width(int *flags_ptr)
{
	int				termwidth;
	int				flags;
	struct winsize	win;

	termwidth = 0;
	flags = *flags_ptr;
	if (isatty(STDOUT_FILENO))
	{
		if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &win) != -1 && win.ws_col > 0)
			termwidth = win.ws_col;
		flags |= F_COLORS;
		if (!(flags & F_OCTAL) && !(flags & F_OCTAL_ESC))
			flags |= F_NONPRINT;
	}
	else
		flags |= F_SINGLECOL;
	*flags_ptr = flags;
	return (termwidth);
}

/*
** If more than one operand is given, non-directory operands are displayed
** first; directory and non-directory operands are sorted
** separately and in lexicographical order.
*/

int			main(int argc, char **argv)
{
	int			flags;
	t_operands	*ops;
	int			tw;
	int			count_opt;

	count_opt = 0;
	flags = argc < 2 ? 0 : get_options(argc, argv, &count_opt);
	tw = ls_col_width(&flags);
	if (!(flags & F_NOSORT))
		ls_sort_params(argv, argc, count_opt);
	ops = get_ops(argv, argc, flags, count_opt);
	err_ops(&ops, count_opt);
	traverse_ops(ops, flags, tw);
	delete_ops_list(&ops);
	return (0);
}
