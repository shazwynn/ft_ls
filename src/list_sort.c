/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_sort.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/20 18:38:46 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 21:38:49 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int	swap_nodes(t_stuff **h, t_stuff *p, t_stuff *c, t_stuff *a)
{
	if (p)
		p->next = a;
	else
		*h = a;
	c->next = a->next;
	a->next = c;
	return (1);
}

static void	rev_swap_nodes(t_stuff **head, int i_start, int i_end)
{
	t_stuff	*tmp;
	t_stuff	*start;
	t_stuff	*end;
	t_stuff	*start_prev;
	t_stuff	*end_prev;

	start = get_node(head, i_start);
	end_prev = get_node(head, i_end - 1);
	end = end_prev->next;
	tmp = end->next;
	if (i_start)
	{
		start_prev = get_node(head, i_start - 1);
		start_prev->next = end;
	}
	else
		*head = end;
	if (i_end != i_start + 1)
	{
		end->next = start->next;
		end_prev->next = start;
	}
	else
		end->next = start;
	start->next = tmp;
}

void		list_sort_reverse(t_stuff **head)
{
	int		size;
	int		i;
	int		i_start;
	int		i_end;
	t_stuff	*tmp;

	tmp = *head;
	size = 0;
	while (tmp && ++size)
		tmp = tmp->next;
	i = size / 2;
	i_start = 0;
	i_end = size - 1;
	while (size > 1 && i)
	{
		rev_swap_nodes(head, i_start++, i_end--);
		i--;
	}
	tmp = *head;
	while (tmp)
	{
		if (tmp->side_next)
			list_sort_reverse(&(tmp->side_next));
		tmp = tmp->next;
	}
}

void		list_sort_by(t_stuff **head, long param, int fl)
{
	t_stuff	*p;
	t_stuff	*c;
	t_stuff	*a;
	int		sorted;

	sorted = 1;
	c = NULL;
	while (sorted)
	{
		sorted = 0;
		p = NULL;
		c = find_first_node(*head, fl) ? find_first_node(*head, fl) : *head;
		a = c->next;
		while (a)
		{
			if ((param && c->ntime && c->ntime < a->time) || (param &&
						c->time < a->time) || (!param && c->size < a->size))
				sorted += swap_nodes(head, p, c, a);
			if (c->side_next)
				list_sort_by(&(c->side_next), param, fl);
			p = c;
			c = c->next;
			a = c ? c->next : NULL;
		}
	}
}

void		list_sort_alpha(t_stuff **head)
{
	t_stuff	*prev;
	t_stuff	*curr;
	t_stuff	*after;
	int		sorted;

	sorted = 1;
	while (sorted)
	{
		sorted = 0;
		prev = NULL;
		curr = *head;
		after = curr->next;
		while (after)
		{
			if (ft_strcmp(curr->name, after->name) > 0)
				sorted += swap_nodes(head, prev, curr, after);
			if (curr->side_next)
				list_sort_alpha(&(curr->side_next));
			prev = curr;
			curr = curr->next;
			after = curr ? curr->next : NULL;
		}
	}
}
