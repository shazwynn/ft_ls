/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   opt_to_flags.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/16 18:26:39 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 22:05:53 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

/*
** set flag : flags = flags | F_x
** unset flag : flags = flags & ~F_x
** multiple operations : myflags |= (option4 | option5)
** myflags &= ~(option4 | option5)
** myflags ^= option4; // flip option4 from on to off, or vice versa
** could go by flag instead and use ternaries
** flags |= (c_in_macro('option', "lbdf") ? F_NONPRINT : F_ACCESSTIME)
**	if (option == '@')
**		flags |= F_EXTATT;
**	else if (option == 'A')
**		flags |= F_LISTDOT;// there is no flag to deal with -A !
*/

static int	opt_to_flags_1(char option, int flags)
{
	if (option == '1')
	{
		flags |= F_SINGLECOL;
		flags &= ~(F_LONGFORM | F_STREAM);
	}
	else if (option == 'a' || option == 'A')
		flags |= F_LISTDOT;
	else if (option == 'B')
	{
		flags |= F_OCTAL;
		flags &= ~(F_NONPRINT | F_OCTAL_ESC);
	}
	else if (option == 'b')
	{
		flags |= F_OCTAL_ESC;
		flags &= ~(F_NONPRINT | F_OCTAL);
	}
	else if (option == 'c')
	{
		flags |= F_STATUSTIME;
		flags &= ~(F_ACCESSTIME | F_CREATTIME);
	}
	return (flags);
}

/*
**	else if (option == 'e')
**		flags |= F_ACL;
*/

static int	opt_to_flags_2(char option, int flags)
{
	if (option == 'd')
	{
		flags |= F_LISTDIR;
		flags |= F_LISTDOT;
		flags &= ~F_RECURSIVE;
	}
	else if (option == 'F')
	{
		flags |= F_TYPE;
		flags &= ~F_SLASH;
	}
	else if (option == 'f')
		flags |= (F_LISTDOT | F_NOSORT);
	else if (option == 'G')
		flags |= (F_COLORS);
	else if (option == 'g')
		flags |= (F_LONGFORM | F_GROUPONLY);
	else if (option == 'i')
		flags |= F_INODE;
	else if (option == 'k')
		flags |= F_KBLOCKS;
	return (flags);
}

/*
**	if (option == 'h')
**		flags |= F_HUMANVAL;
**	else if (option == 'O')
**		flags |= F_FLAGS;
**	else if (option == 'm')
**	{
**		flags |= F_STREAM;
**		flags &= ~(F_SINGLECOL | F_LONGFORM);
**	}
*/

static int	opt_to_flags_3(char option, int flags)
{
	if (option == 'l')
	{
		flags |= F_LONGFORM;
		flags &= ~(F_SINGLECOL | F_STREAM);
	}
	else if (option == 'n')
		flags |= (F_LONGFORM | F_NUMERICONLY);
	else if (option == 'o')
		flags |= (F_LONGFORM | F_NOGROUP);
	else if (option == 'p')
	{
		flags |= F_SLASH;
		flags &= ~F_TYPE;
	}
	else if (option == 'q')
	{
		flags |= F_NONPRINT;
		flags &= ~(F_OCTAL_ESC | F_OCTAL);
	}
	else if (option == 'R')
		flags |= F_RECURSIVE;
	else if (option == 'r')
		flags |= F_REVERSESORT;
	return (flags);
}

/*
**	else if (option == 'x')
**	{
**		flags |= F_SORTACROSS;
**		flags &= ~(F_SINGLECOL | F_LONGFORM);
**	}
*/

static int	opt_to_flags_4(char option, int flags)
{
	if (option == 'S')
		flags |= F_SIZESORT;
	else if (option == 's')
		flags |= F_NBLOCKS;
	else if (option == 'T')
		flags |= F_SECTIME;
	else if (option == 't')
		flags |= F_TIMESORT;
	else if (option == 'u')
	{
		flags |= F_ACCESSTIME;
		flags &= ~(F_STATUSTIME | F_CREATTIME);
	}
	else if (option == 'U')
	{
		flags |= F_CREATTIME;
		flags &= ~(F_STATUSTIME | F_ACCESSTIME);
	}
	return (flags);
}

/*
** forcing single column output for options with no column padding management
*/

int			opt_to_flags(char option, int flags)
{
	flags = opt_to_flags_1(option, flags);
	flags = opt_to_flags_2(option, flags);
	flags = opt_to_flags_3(option, flags);
	flags = opt_to_flags_4(option, flags);
	if (!(flags & F_LONGFORM))
	{
		if (flags & F_INODE || flags & F_NBLOCKS)
			flags |= F_SINGLECOL;
		if (flags & F_OCTAL || flags & F_OCTAL_ESC)
			flags |= F_SINGLECOL;
	}
	return (flags);
}
