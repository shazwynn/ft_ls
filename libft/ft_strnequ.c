/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 19:49:10 by algrele           #+#    #+#             */
/*   Updated: 2018/04/04 20:11:50 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

int	ft_strnequ(char const *s1, char const *s2, size_t n)
{
	size_t	i;

	i = 0;
	if (!s1 || !s2)
		return (FALSE);
	while (s1[i] && s2[i] && s1[i] == s2[i] && i < n)
		i++;
	if (i != n && s1[i] && s2[i])
		return (FALSE);
	else
		return (TRUE);
}
