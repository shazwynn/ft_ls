/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_replace.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 21:05:43 by algrele           #+#    #+#             */
/*   Updated: 2018/04/06 21:08:26 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	ft_putstr_replace(char *str, char c, char rep)
{
	int	i;

	i = 0;
	while (str[i])
	{
		if (str[i] == c)
			ft_putchar(rep);
		else
			ft_putchar(str[i]);
		i++;
	}
}
