/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 19:37:56 by algrele           #+#    #+#             */
/*   Updated: 2018/04/07 00:49:07 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static int	count_words(char const *s, char c)
{
	int	i;
	int count;

	i = 0;
	count = 0;
	while (s[i])
	{
		if (s[i] == c)
			i++;
		if (s[i] != c && s[i])
		{
			while (s[i] && s[i] != c)
				i++;
			count++;
		}
	}
	return (count);
}

static char	**free_tab(char **tab)
{
	int	i;

	i = 0;
	while (tab[i])
	{
		free(tab[i]);
		i++;
	}
	free(tab);
	return (tab);
}

char		**ft_strsplit(char const *str, char c)
{
	char	**tab;
	int		i;
	int		skip;
	int		j;

	i = 0;
	j = 0;
	if (!str)
		return (NULL);
	if (!(tab = (char **)malloc(sizeof(char *) * (count_words(str, c) + 1))))
		return (NULL);
	while (str && str[i])
	{
		skip = 0;
		while (str[i] && str[i] == c)
			i++;
		while (str[i + skip] && str[i + skip] != c)
			skip++;
		if (skip > 0)
			if (!(tab[j++] = ft_strsub(str, i, skip)))
				return (free_tab(tab));
		i = i + skip;
	}
	tab[j] = 0;
	return (tab);
}
