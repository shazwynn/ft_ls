/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 20:55:39 by algrele           #+#    #+#             */
/*   Updated: 2018/04/06 20:57:29 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static int	ft_check_base(char *base)
{
	int	i;
	int j;

	i = 0;
	j = 0;
	if (!base || !base[i])
		return (0);
	while (base[i])
	{
		if (base[i] == '+' || base[i] == '-')
			return (0);
		i++;
	}
	if (i == 1)
		return (0);
	i = 0;
	while (base[i])
	{
		j = i + 1;
		while (base[j])
			if (base[i] == base[j++])
				return (0);
		i++;
	}
	return (1);
}

static void	ft_recursive_number(long int nb, char *base, long int i_base)
{
	if (nb < 0)
	{
		ft_putchar('-');
		nb = -nb;
	}
	if (nb >= i_base)
		ft_recursive_number(nb / i_base, base, i_base);
	ft_putchar(base[nb % i_base]);
}

void		ft_putnbr_base(int nb, char *base)
{
	int		stock;
	int		i_base;

	if (ft_check_base(base))
	{
		i_base = 0;
		while (base && base[i_base])
			i_base++;
		stock = 0;
		ft_recursive_number(nb, base, i_base);
	}
}
