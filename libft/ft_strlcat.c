/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 16:11:31 by algrele           #+#    #+#             */
/*   Updated: 2018/04/04 17:23:56 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** as long as size > 0 or as long as there is one byte free in dst, result
** will be nul terminated. a byte for the '\0' is added in size
** so we append at most size - strlen(dest) - 1.
** src and original dst are supposed to be c strings and thus nul terminated.
** ret : total len of str it tried to create (initial len of dst + len of src)
** if strlcat traverses size chars and finds no nul the len of the string is
** considered to be size and dst will not be nul terminated (no space for it)
*/

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	i;
	size_t	j;
	size_t	len;

	i = 0;
	j = size;
	len = ft_strlen(dst);
	if (len > size)
		len = size;
	i = -1;
	while (src[++i] && j-- > len + 1)
		dst[i + len] = src[i];
	if (size > i + len)
		dst[i + len] = '\0';
	return (ft_strlen(src) + len);
}
