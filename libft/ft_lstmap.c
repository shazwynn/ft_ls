/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 22:55:10 by algrele           #+#    #+#             */
/*   Updated: 2018/04/06 23:55:46 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*head;
	t_list	*new;
	t_list	*findlast;

	head = NULL;
	if (!lst || !f)
		return (NULL);
	while (lst)
	{
		if (!(new = ft_lstnew(lst->content, lst->content_size)))
			return (NULL);
		new = f(new);
		if (head)
		{
			findlast = head;
			while (findlast->next)
				findlast = findlast->next;
			findlast->next = new;
		}
		else
			head = new;
		lst = lst->next;
	}
	return (head);
}
