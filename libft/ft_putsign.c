/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putsign.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 14:46:35 by algrele           #+#    #+#             */
/*   Updated: 2018/04/04 14:50:11 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** PUTSTR "ZERO", "POSITIVE", "NEGATIVE" depending on number sign
*/

void	ft_putsign(int n)
{
	ft_putsign_fd(n, 1);
}
