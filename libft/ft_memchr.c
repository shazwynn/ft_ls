/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/06 17:02:43 by algrele           #+#    #+#             */
/*   Updated: 2018/04/06 17:03:19 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	*ft_memchr(const void *s, int c, size_t n)
{
	size_t			i;
	unsigned char	occ;

	i = 0;
	while (i < n)
	{
		occ = ((const char *)s)[i];
		if (occ == (unsigned char)c)
			return ((void *)s + i);
		i++;
	}
	return (NULL);
}
