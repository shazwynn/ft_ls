/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 17:29:19 by algrele           #+#    #+#             */
/*   Updated: 2018/04/04 17:37:45 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** locates the first occurence of needle in haystack. No more than len
** characters are searched. if needle is empty, ret haystack.
** if no occurence of needle in haystack, ret NULL. else, ret a pointer
** to the first occurence of needle.
*/

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	size_t	i;
	size_t	j;

	i = 0;
	if (!needle[i])
		return ((char *)haystack);
	else
	{
		while (haystack[i])
		{
			j = 0;
			while (haystack[i + j] == needle[j])
			{
				if (needle[j + 1] == '\0' && i + j < len)
					return ((char *)haystack + i);
				j++;
			}
			i++;
		}
	}
	return (NULL);
}
