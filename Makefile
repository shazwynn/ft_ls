# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: algrele <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/03 22:30:52 by algrele           #+#    #+#              #
#    Updated: 2019/03/17 23:44:55 by algrele          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_ls

CC = gcc
CFLAGS = -Wall -Wextra -Werror
#-fsanitize=address -g3

LFT_LIB = -L libft -lft
LFT_DIR = ./libft
PF_LIB = -L ft_printf -lftprintf
PF_DIR = ./ft_printf
PFI_DIR = ./ft_printf/includes
SRC_DIR = ./src/
OBJ_DIR = ./obj/
LSI_DIR = ./
INCLUDES = -I $(PFI_DIR) -I $(LFT_DIR) -I $(LSI_DIR)

_SRCS = display_tools.c \
		display_pr.c \
		display_mc.c \
		display_np.c \
		display_isk_type.c \
		display_long.c \
		display_name.c \
		list_tools.c \
		error_handling.c \
		traverse.c \
		list_sort.c \
		main.c \
		ops_list.c \
		get_options.c \
		opt_to_flags.c \
		struct_tools.c \
		ft_sort_params.c \
		create_list.c \
		create_list_files.c \
		free_stuff.c \
		ft_joinpath.c \
		struct_padding.c \
		display_permissions.c
#		ls_debug.c

SRCS = $(addprefix $(SRC_DIR), $(_SRCS))
OBJS = $(addprefix $(OBJ_DIR), $(_SRCS:.c=.o))

all: $(NAME)

$(NAME): $(OBJS) ft_ls.h ft_ls_structs.h
	@make -C $(LFT_DIR)
	@make -C $(PF_DIR)
	@$(CC) $(CFLAGS) $(OBJS) $(INCLUDES) $(PF_LIB) $(LFT_LIB) -o $(NAME)

	@echo "\033[1;34m""\033[5m"
	@echo " _______  _______          ___      _______ "
	@echo "|FT_LS_F||T_LS_FT|        |_LS|    |_FT_LS_|"
	@echo "|FT_LS_F||T_LS_FT|        |_LS|    |_FT_LS_|"
	@echo "|FT_|___   |LS_|          |FT_|    |L|_____ "
	@echo "|S_FT_LS|  |_FT|          |_LS|___ |_FT_LS_|"
	@echo "|FT_|      |LS_|   _____  |FT_LS_F| _____|T|"
	@echo "|_LS|      |_FT|  |_LS_F| |T_LS_FT||_LS_FT_|"
	@echo "\033[0m"

$(OBJ_DIR)%.o: $(SRC_DIR)%.c
	@$(CC) -c $(CFLAGS) $(INCLUDES) $< -o $@


clean:
	@rm -f $(OBJS)
	$(MAKE) -C $(LFT_DIR) clean
	$(MAKE) -C $(PF_DIR) clean

fclean : clean
	@rm -f $(NAME)
	$(MAKE) -C $(LFT_DIR) fclean
	$(MAKE) -C $(PF_DIR) fclean

re : fclean all

.PHONY: all clean fclean re libft ft_printf
